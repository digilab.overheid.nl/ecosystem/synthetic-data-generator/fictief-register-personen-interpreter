---
title : "Fictief register personen interpreter"
description: "Documentation for the personal records interpreter"
lead: ""
date: 2023-08-28T14:52:40+02:00
draft: true
toc: true
---

## Running locally
Clone [this repo](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen).

To run the development server:

Default settings are:
- Nats host: localhost:4222
- Base URL of records db: localhost:9010

Run:
```shell
go run . interpreter
```

To overwrite the default settings

Run:
```shell
go run . interpreter --nats-host-address=127.0.0.1 --nats-port=4222 --base-url=http://127.0.0.1:9010
```
