package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

func (app *Application) HandleBirthEvent(ctx context.Context, event *model.Event) error {
	data := new(model.BirthEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if app.Filter(data.PlaceOfBirth.Code) {
		return nil
	}

	person := model.BirthEvent{
		ID:     event.SubjectIDs[0],
		BSN:    fmt.Sprintf("%d", data.BSN),
		BornAt: event.OccurredAt,
		Sex:    data.Gender,
	}

	if err := app.Request(ctx, http.MethodPost, "/people", person, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleNamingEvent(ctx context.Context, event *model.Event) error {
	data := new(model.NamingEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if app.Filter(data.Municipality.Code) {
		return nil
	}

	namingEvent := model.NamingEvent{
		Name: fmt.Sprintf("%s %s", data.GivenName, data.Surname),
	}

	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0].String()), namingEvent, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleDeathEvent(ctx context.Context, event *model.Event) error {
	deathEvent := model.DeathEvent{
		DiedAt: event.OccurredAt,
	}

	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), deathEvent, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleRegisteredOnAddressEvent(ctx context.Context, event *model.Event) error {
	data := new(model.RegisteredOnAddressEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if app.Filter(data.Municipality.Code) {
		return nil
	}

	addressRegisteredEvent := model.AddressChangedEvent{
		RegisterAddressID: data.AddressID,
	}

	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), addressRegisteredEvent, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleAddressWrittenOutEvent(ctx context.Context, event *model.Event) error {
	addressWrittenOutEvent := model.AddressChangedEvent{
		RegisterAddressID: uuid.UUID{},
	}

	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), addressWrittenOutEvent, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}
