package application

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"

	"github.com/nats-io/nats.go"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

type Application struct {
	client           *http.Client
	baseURL          string
	municipalityCode string
}

var ErrResponseFailed = errors.New("response failed with")

func NewApplication(baseURL, municipalityCode string) *Application {
	return &Application{
		client:           http.DefaultClient,
		baseURL:          baseURL,
		municipalityCode: municipalityCode,
	}
}

func (app *Application) HandleEvents(msg *nats.Msg) {
	event := new(model.Event)
	if err := json.Unmarshal(msg.Data, event); err != nil {
		fmt.Print(fmt.Errorf("unmarshal failed: %w", err))
	}

	switch event.EventType {
	case "GeboorteVastgesteld":
		if err := app.HandleBirthEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle birth event failed: %w", err))
		}
	case "NamenVastgesteld":
		if err := app.HandleNamingEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle naming event failed: %w", err))
		}
	case "OverlijdenVastgesteld":
		if err := app.HandleDeathEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle death event failed: %w", err))
		}
	case "AdresIngeschreven":
		if err := app.HandleRegisteredOnAddressEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle address registered event failed: %w", err))
		}
	case "AdresUitgeschreven":
		if err := app.HandleAddressWrittenOutEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle address written out event failed: %w", err))
		}
	default:
		fmt.Printf("Event type: %s unknown\n", event.EventType)
	}
}

func (app *Application) Request(ctx context.Context, method, path string, data, value any) error {
	buf := new(bytes.Buffer)
	if data != nil {
		if err := json.NewEncoder(buf).Encode(data); err != nil {
			return fmt.Errorf("encoding failed: %w", err)
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, app.baseURL+path, buf)
	if err != nil {
		return fmt.Errorf("new request failed: %w", err)
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	resp, err := app.client.Do(req)
	if err != nil {
		return fmt.Errorf("new request failed: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, _ := httputil.DumpResponse(resp, true)

		return fmt.Errorf("%w: %s", ErrResponseFailed, string(body))
	}

	if value != nil {
		if err := json.NewDecoder(resp.Body).Decode(value); err != nil {
			return fmt.Errorf("decoding failed: %w", err)
		}
	}

	return nil
}

// Filter
// Filter will check if the given municipality equals the configuration based municipality
// If the configured municipality is empty we dont filter at all
//
// Return val true -> remove value;
// Return val false -> keep value.
func (app *Application) Filter(municipalityCode string) bool {
	if app.municipalityCode == "" {
		return false
	}

	if app.municipalityCode == municipalityCode {
		return false
	}

	return true
}
