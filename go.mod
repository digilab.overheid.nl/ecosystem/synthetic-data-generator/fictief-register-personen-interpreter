module gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter

go 1.21.0

require (
	github.com/google/uuid v1.3.1
	github.com/nats-io/nats.go v1.28.0
	github.com/spf13/cobra v1.7.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/nats-io/nats-server/v2 v2.9.21 // indirect
	github.com/nats-io/nkeys v0.4.4 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.11.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
