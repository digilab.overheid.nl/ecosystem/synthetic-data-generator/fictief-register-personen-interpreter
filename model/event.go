package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}

type Municipality struct {
	Name string `json:"name"`
	Code string `json:"code"`
}

type BirthEventData struct {
	BSN          int          `json:"nationalInsuranceNumber"`
	PlaceOfBirth Municipality `json:"placeOfBirth"`
	Gender       string       `json:"gender"`
}

type NamingEventData struct {
	GivenName    string       `json:"givenName"`
	Surname      string       `json:"surName"`
	Municipality Municipality `json:"municipality"`
}

type BirthEvent struct {
	ID     uuid.UUID `json:"id"`
	BSN    string    `json:"bsn"`
	BornAt time.Time `json:"bornAt"`
	Sex    string    `json:"sex"`
}

type NamingEvent struct {
	Name string `json:"name"`
}

type DeathEvent struct {
	DiedAt time.Time `json:"diedAt"`
}

type RegisteredOnAddressEventData struct {
	AddressID    uuid.UUID    `json:"address"`
	Municipality Municipality `json:"municipality"`
}

type AddressChangedEvent struct {
	RegisterAddressID uuid.UUID `json:"registeredAddressId"`
}
