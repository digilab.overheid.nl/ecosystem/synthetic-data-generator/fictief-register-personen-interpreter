package cmd

import (
	"fmt"
	"log"
	"net"

	"github.com/nats-io/nats.go"
	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/process"
)

var interpreterOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	Nats struct {
		Host string
		Port string
	}
	BaseURL      string
	Municipality string
}

func init() { //nolint:gochecknoinits // this is the recommended way to use cobra
	interpreterCommand.Flags().StringVarP(&interpreterOpts.Nats.Host, "sdg-nats-service-host", "", "127.0.0.1", "Address of NATS")
	interpreterCommand.Flags().StringVarP(&interpreterOpts.Nats.Port, "sdg-nats-service-port", "", "4222", "Port of NATS")

	interpreterCommand.Flags().StringVarP(&interpreterOpts.BaseURL,
		"sdg-backend-url", "", "http://0.0.0.0:9010", "Base URL of the Fictieve Register")

	interpreterCommand.Flags().StringVarP(&interpreterOpts.Municipality,
		"sdg-municipality-code", "", "", "Municipality Code that this interpreter is working for")
}

var interpreterCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "interpreter",
	Short: "Run the event source tool",
	Run: func(cmd *cobra.Command, args []string) {
		proc := process.NewProcess()

		conn, err := nats.Connect(fmt.Sprintf("nats://%s", net.JoinHostPort(interpreterOpts.Nats.Host, interpreterOpts.Nats.Port)))
		if err != nil {
			log.Fatal(fmt.Errorf("failed to connect to nats: %w", err))
		}

		fmt.Println("Starting")

		stream, err := conn.JetStream()
		if err != nil {
			log.Fatal(err)
		}

		app := application.NewApplication(interpreterOpts.BaseURL, interpreterOpts.Municipality)

		sub, err := stream.Subscribe("events.frp.>", app.HandleEvents)
		if err != nil {
			log.Fatal(err)
		}

		proc.Wait()

		if err := sub.Unsubscribe(); err != nil {
			fmt.Print(fmt.Errorf("unsubscribe failed: %w", err))
		}

		if err := sub.Drain(); err != nil {
			fmt.Print(fmt.Errorf("subscription drain failed: %w", err))
		}

		if err := conn.Drain(); err != nil {
			fmt.Print(fmt.Errorf("connectoin drain failed: %w", err))
		}

		conn.Close()
	},
}
