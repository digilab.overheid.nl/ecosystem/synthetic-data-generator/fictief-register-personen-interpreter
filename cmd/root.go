package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "api",
	Short: "FRP Interpreter",
}

func Execute() error {
	return RootCmd.Execute() //nolint:wrapcheck // not necessary
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	RootCmd.AddCommand(interpreterCommand)
}
